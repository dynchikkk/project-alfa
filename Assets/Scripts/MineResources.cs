﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineResources : MonoBehaviour
{
    public event Action OnMineResoursesChanged;

    public int _resourecesInStack = 1000;
    [HideInInspector] public int _currentResourecesInStack;

    private void Start()
    {
        _currentResourecesInStack = _resourecesInStack;
        OnMineResoursesChanged?.Invoke();
    }

    //Give resource to
    public void TakeResources(int _kol)
    {
        if (Player._main._takenResources >= Player._main._maxTakenResources)
            return;

        int howMuchResoursesPlayerCanTake = Player._main._maxTakenResources - Player._main._takenResources;
        if (_kol > howMuchResoursesPlayerCanTake)
            _kol = howMuchResoursesPlayerCanTake;

        if (_currentResourecesInStack - _kol > 0)
        {
            _currentResourecesInStack -= _kol;
            Player._main.TakeResources(_kol);
        }
        else if (_currentResourecesInStack - _kol <= 0)
        {
            Player._main.TakeResources(_currentResourecesInStack);
            _currentResourecesInStack = 0;
        }

        OnMineResoursesChanged?.Invoke();

        if (_currentResourecesInStack <= 0)
        {
            Destroy(gameObject);
        }
        print(_currentResourecesInStack);
    }
}
