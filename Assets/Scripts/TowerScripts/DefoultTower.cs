﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DefoultTower : Tower
{
    public GameObject _bullet;
    
    public GameObject _target;
    public GameObject _bulletTemplate;

    protected override void Start()
    {
        ChooseTarget();
    }

    private void Update()
    {
        if (_isBuild is false)
            return;

        _attackCd -= Time.deltaTime;

        ChooseTarget();
        
        if(_target != null)
        {
            FaceTarget(_target.transform.position);

            if (_attackCd <= 0)
            {
                Attack(_target);
            }
        } 
    }

    private void FaceTarget(Vector3 destination)
    {
        Vector3 lookPos = destination - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, _towerConfig.defenceTowerCharacteristics._rotatoinSpeed);
    }

    public void ChooseTarget()
    {
        //Find all targets in _attck zone
        var _targets = Physics.OverlapSphere(transform.position, _towerConfig.defenceTowerCharacteristics._attackZone);

        for (int i = 0; i < _targets.Length; i++)
        {
            //make a ray that check if turel see an enemy
            Ray VievRay = new Ray(transform.position, _targets[i].transform.position - transform.position);
            RaycastHit hit;
            if (Physics.Raycast(VievRay, out hit, _towerConfig.defenceTowerCharacteristics._attackZone))
            {
                //if hit enemy
                if(hit.transform.gameObject.tag == "Enemy")
                {
                    _target = hit.transform.gameObject;
                }
            }
        }
    }

    public void Attack(GameObject _target)
    {
        if (_currentResources - _towerConfig.defenceTowerCharacteristics._resourcesForShot >= 0)
        {
            //Fire
            GameObject link = Instantiate(_bullet, _bulletTemplate.transform);
            link.GetComponent<Bullet>()._speed = _towerConfig.defenceTowerCharacteristics._firePower;
            link.GetComponent<Bullet>()._direction = transform.forward;
            link.GetComponent<Bullet>()._damage = _towerConfig.defenceTowerCharacteristics._damage;

            _attackCd = _towerConfig.defenceTowerCharacteristics._attackCd;
            _currentResources -= _towerConfig.defenceTowerCharacteristics._resourcesForShot;
            base.InitializeResources();
        }
    }
}


