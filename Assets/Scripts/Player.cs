﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public static Player _main;

    public event Action OnPlayerResoursesChanged;

    [Header("Player constants")]
    //Max player hp
    //public float _hp = 100;
    //[HideInInspector] public float _currentHp;
    [SerializeField] private float _moveSpeed = 10;
    public float _damage = 100;
    //Max enemy attack cooldown
    public float _damageCd = 2;
    [HideInInspector] public float _currentDamageCd;
    //Start Position of Player
    public Vector3 _startPosition = new Vector3(15, 0, 10);

    //Resources
    public int _maxTakenResources = 100;
    public int _takenResources = 0;
    public int _resourceDamage;
    public float _recourceGiveCd = 0.1f;
    [HideInInspector] public float _currentRecourceGiveCd;

    //Camera
    private Camera _cam;
    public Vector3 _camOffset;

    // InputSystem
    private PlayerInput _playerInputSystem;

    private Health _health;

    private void Awake()
    {
        _main = this;
        _playerInputSystem = new PlayerInput();
        _health = GetComponent<Health>();
        _cam = Camera.main;
    }

    private void Start()
    {
        SetStartCharacteristic();
        AssignCamera();

        OnPlayerResoursesChanged?.Invoke();
    }

    private void OnEnable()
    {
        //print(_cam.transform.position - transform.position); //if need to change offset 
        _playerInputSystem.Enable();

        _health.OnPersonDead += Death;
    }

    private void OnDisable()
    {
        _playerInputSystem.Disable();

        _health.OnPersonDead -= Death;
    }

    private void Update()
    {
        Move();
        AssignCamera();
        _currentDamageCd -= Time.deltaTime;
        _currentRecourceGiveCd -= Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        Attack(other.gameObject);
    }

    private void OnTriggerStay(Collider other)
    {
        Attack(other.gameObject);
    }

    public void SetStartCharacteristic()
    {
        transform.position = _startPosition;
        _currentDamageCd = 0;
        _currentRecourceGiveCd = 0;
    }

    private void Move()
    {
        //Direction
        Vector2 _moveDirection = _playerInputSystem.Player.Move.ReadValue<Vector2>();

        //Rotation player
        Vector3 _move = new Vector3(_moveDirection.x, 0, _moveDirection.y);
        if (_move != Vector3.zero)
        {
            transform.forward = _move;
        }

        //Move
        GetComponent<Rigidbody>().velocity = _move * Time.deltaTime * _moveSpeed;
    }

    private void AssignCamera()
    {
        _cam.transform.position = transform.position + _camOffset;
    }

    public void Death()
    {
        GameObject _link = Instantiate(gameObject);
        _link.transform.position = _startPosition;
        Game._main._player = _link;
        Destroy(gameObject);
    }

    private void Attack(GameObject _target)
    {
        if (_currentDamageCd <= 0)
        {
            //this flag checks if at least one condition is met
            bool fl = false;
            if (_target.tag == "Enemy")
            {
                _target.GetComponent<Health>().TakeDamage(_damage);
                fl = true;
            }
            if (_target.tag == "Resources")
            {
                _target.GetComponent<MineResources>().TakeResources(_resourceDamage);
                fl = true;
            }

            if (fl)
            {
                _currentDamageCd = _damageCd;
            }
        }

        //if touch tower
        if (_currentRecourceGiveCd <= 0 && _target.tag == "Defense")
        {
            GiveResources(_target, 1);
        }
    }

    //Take resource from
    public void TakeResources(int _kol)
    {
        _takenResources += _kol;
        if(_takenResources >= _maxTakenResources)
        {
            _takenResources = _maxTakenResources;
        }

        OnPlayerResoursesChanged?.Invoke();
    }

    //Give resources to
    public void GiveResources(GameObject _target, int _kol)
    {
        if (_takenResources - _kol >= 0)
        {
            if (_target.tag == "Defense")
                _target.GetComponent<Tower>().TakeResources(_kol);

            _takenResources -= _kol;

            _currentRecourceGiveCd = _recourceGiveCd;
        }

        OnPlayerResoursesChanged?.Invoke();
    }
}
