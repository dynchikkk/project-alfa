﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MeleeEnemy : StandartEnemy
{
    protected override void Start()
    {
        base.Start();
        ChangeTarget();
    }

    private void Update()
    {
        ChangeTarget();
        _currentDamageCd -= Time.deltaTime;
    }

    private void OnTriggerStay(Collider other)
    {
        AttackTarget(other.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        AttackTarget(other.gameObject);
    }
    

    public void ChangeTarget()
    {
        if (Vector3.Distance(transform.position, _localMain._player.transform.position) > _enemyConfig.enemyCharacteristics._attackZone)
        {
            _agent.SetDestination(_localMain._mainHouse.transform.position);
        }
        else
        {
            _agent.SetDestination(_localMain._player.transform.position);
        }
    }

    public void AttackTarget(GameObject _target)
    {
        if (_currentDamageCd <= 0)
        {
            //this flag checks if at least one condition is met
            bool fl = false;
            if (_target.tag == "Player")
            {
                //_target.GetComponent<Player>().TakeDamage(_enemyConfig.enemyCharacteristics._damage);
                _target.GetComponent<Health>().TakeDamage(_enemyConfig.enemyCharacteristics._damage);
                //Replusion(_target);
                fl = true;
            }
            if (_target.tag == "MainHouse")
            {
                //_target.GetComponent<MainHouse>().TakeDamage(_enemyConfig.enemyCharacteristics._damage);
                _target.GetComponent<Health>().TakeDamage(_enemyConfig.enemyCharacteristics._damage);
                fl = true;
            }

            if(fl)
            {
                _currentDamageCd = _enemyConfig.enemyCharacteristics._damageCd;
            }
        }
    }
    
}
