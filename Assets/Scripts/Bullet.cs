﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float _speed;
    public float _damage;
    public Vector3 _direction;
    public float timeToDestroy;

    private void Start()
    {
        transform.parent = null;
    }

    private void Update()
    {
        timeToDestroy -= Time.deltaTime;

        if (timeToDestroy<=0)
            Destroy(gameObject);

        if (_direction.x != 0 && _direction.z != 0)
            Fly();
    }

    public void Fly()
    {
        GetComponent<Rigidbody>().velocity = _direction.normalized * _speed * Time.deltaTime;
    }

    public void Attack(GameObject _target)
    {
        if(_target.tag == "Enemy")
        {
            _target.GetComponent<Health>().TakeDamage(_damage);
            Destroy(gameObject);
        }

        
    }

    private void OnTriggerEnter(Collider other)
    {
        Attack(other.gameObject);      
    }
}
