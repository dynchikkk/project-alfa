﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Scriot for tower spawner
public class TowerPlace : MonoBehaviour
{
    Game _localMain;
    public int _spawnerNum;

    private void Start()
    {
        _localMain = Game._main;
    }

    public void InstantiateTower(int _towerNum)
    {
        GameObject _link = Instantiate(_localMain._towersType[_towerNum], transform);
        _link.GetComponent<Tower>()._towerConfig.defenceTowerCharacteristics._spawner = _spawnerNum;
        _localMain.DestroyTowerButtons();
        VisibleOfGameObject(false);
    }

    public void VisibleOfGameObject(bool ans)
    {
        gameObject.GetComponent<Collider>().enabled = ans;
        gameObject.GetComponent<MeshRenderer>().enabled = ans;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            _localMain._lastTowerSpawner = gameObject;
            _localMain.CreateTowerButtons();
        }  
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            _localMain.DestroyTowerButtons();
        }
    }
}
