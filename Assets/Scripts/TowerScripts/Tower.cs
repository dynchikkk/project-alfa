﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Tower : MonoBehaviour
{
    public event Action OnTowerBuilt;
    public event Action OnTowerResoursesChanged;

    public DefenceTowerConfig _towerConfig;
    
    public float _attackCd;
    public float _currentResources;

    public bool _isBuild;

    protected virtual void Start()
    {
        SetStartCharacteristic();
    }
    
    public void SetStartCharacteristic()
    {
        _attackCd = 0;
        _currentResources = 0;
        _isBuild = false;
        InitializeResources();
    }
    

    //Take resources from
    public void TakeResources(int _kol)
    {
        _currentResources += _kol;
        InitializeResources();

        //if turel isn't build
        if (_currentResources >= _towerConfig.defenceTowerCharacteristics._resourcesForBuild && _isBuild is false)
        {
            print("build");
            _isBuild = true;
            OnTowerBuilt?.Invoke();

            _currentResources -= _towerConfig.defenceTowerCharacteristics._resourcesForBuild;
            InitializeResources();
        } 
    }

    protected void InitializeResources()
    {
        OnTowerResoursesChanged?.Invoke();
    }
}
