﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthAndResoursesUI : MonoBehaviour
{
    [SerializeField] private TMP_Text _playerHealthText;
    [SerializeField] private TMP_Text _playerResourseText;
    [SerializeField] private TMP_Text _houseHealthText;

    private Health _playerHealth;
    private Player _player;
    private Health _houseHealth;

    private void Awake()
    {
        _playerHealth = Game._main._player.GetComponent<Health>();
        _player = Game._main._player.GetComponent<Player>();
        _houseHealth = Game._main._mainHouse.GetComponent<Health>();
    }

    private void OnEnable()
    {
        _playerHealth.OnHealthChanged += OnPlayerHealthChanged;
        _player.OnPlayerResoursesChanged += OnPlayerResoursesChanged;
        _houseHealth.OnHealthChanged += OnHouseHealthChanged;
    }

    private void OnDisable()
    {
        _playerHealth.OnHealthChanged -= OnPlayerHealthChanged;
        _player.OnPlayerResoursesChanged += OnPlayerResoursesChanged;
        _houseHealth.OnHealthChanged -= OnHouseHealthChanged;
    }

    private void OnHouseHealthChanged()
    {
        _houseHealthText.text = _houseHealth.CurrentHealth.ToString();
    }

    private void OnPlayerResoursesChanged()
    {
        _playerResourseText.text = _player._takenResources.ToString();
    }

    private void OnPlayerHealthChanged()
    {
        _playerHealthText.text = _playerHealth.CurrentHealth.ToString();
    }
}
