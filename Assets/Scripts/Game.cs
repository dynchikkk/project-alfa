﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    public static Game _main;

    [SerializeField] private float _mainTime = 0;

    [Header("Player Attributes")]
    public GameObject _player;
    public GameObject _mainHouse;

    [Header("Enemies Attributes")]
    [SerializeField] private List<GameObject> _enemyTypes = new List<GameObject>();
    private List<GameObject> _allEnemies = new List<GameObject>();
    [SerializeField] private List<GameObject> _enemySpawners = new List<GameObject>();

    [Header("Towers")]
    public List<GameObject> _towersType = new List<GameObject>();
    [SerializeField] private List<GameObject> _towerSpawners = new List<GameObject>();
    [SerializeField] private Button _towerButtonPrefab;
    [SerializeField] private GameObject _towerButtonsParents;
    [HideInInspector] public GameObject _lastTowerSpawner;
    [SerializeField] private float _distanceBetweeenBut;

    private void Awake()
    {
        _main = this;
    }

    private void Update()
    {
        _mainTime += Time.deltaTime;
    }

    private void Start()
    {
        SpawnEnemies(2);
        _distanceBetweeenBut += _towerButtonPrefab.GetComponent<RectTransform>().rect.width;
    }

    public void ChangeScene(string _name)
    {
        SceneManager.LoadScene(_name);
    }

    private void SpawnEnemies(int _countOnSpawner)
    {
        for (int i = 0; i < _enemySpawners.Count; i++)
        {
            for (int j = 0; j < _countOnSpawner; j++)
            {
                GameObject _link = Instantiate(_enemyTypes[Random.Range(0, _enemyTypes.Count)], _enemySpawners[i].transform);
                _allEnemies.Add(_link);
            }
        }
    }

    public void CreateTowerButtons()
    {
        float x = x = -(_distanceBetweeenBut * _towersType.Count / 2) + _distanceBetweeenBut / 2; ;

        for (int i = 0; i < _towersType.Count; i++)
        {
            Button _link = Instantiate(_towerButtonPrefab, _towerButtonsParents.transform);
            _link.transform.localPosition = new Vector3(x, _link.transform.localPosition.y, _link.transform.localPosition.y);
            _link.GetComponentInChildren<Text>().text = $"{_towersType[i].GetComponent<Tower>()._towerConfig.defenceTowerCharacteristics._name}\n" +
                $"{_towersType[i].GetComponent<Tower>()._towerConfig.defenceTowerCharacteristics._resourcesForBuild}";
            _link.GetComponent<TowerPlaceButton>()._towerNum = i;

            x += _distanceBetweeenBut;
        }
    }

    public void DestroyTowerButtons()
    {
        for (int i = 0; i < _towerButtonsParents.transform.childCount; i++)
        {
            Destroy(_towerButtonsParents.transform.GetChild(i).gameObject);
        }
    }
}
