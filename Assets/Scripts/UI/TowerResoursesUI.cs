﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TowerResoursesUI : MonoBehaviour
{
    [SerializeField] private Tower _tower;

    [SerializeField] private Image _whenTowerBuildIcon;

    [SerializeField] private TMP_Text _towerResourse;

    private void OnEnable()
    {
        _tower.OnTowerBuilt += OnTowerBuilt;
        _tower.OnTowerResoursesChanged += OnTowerResoursesChanged;
    }

    private void OnDisable()
    {
        _tower.OnTowerBuilt -= OnTowerBuilt;
        _tower.OnTowerResoursesChanged -= OnTowerResoursesChanged;
    }

    private void OnTowerResoursesChanged()
    {
        if (_tower._isBuild)
        {
            _towerResourse.text = _tower._currentResources.ToString();
        }
        else
        {
            _towerResourse.text = $"{_tower._currentResources}/{_tower._towerConfig.defenceTowerCharacteristics._resourcesForBuild}";
        }
    }

    private void OnTowerBuilt()
    {
        _whenTowerBuildIcon.gameObject.SetActive(false);
    }

}
