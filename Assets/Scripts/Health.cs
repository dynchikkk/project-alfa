﻿using System;
using System.Collections;
using UnityEngine;

public class Health : MonoBehaviour
{
    //Событие при смерти
    public event Action OnPersonDead;
    //Событие при изменении количества hp
    public event Action OnHealthChanged;

    public float MaxHealth;

    public float CurrentHealth;

    private bool isAlive;

    private void Start()
    {
        CurrentHealth = MaxHealth;
        isAlive = true;

        OnHealthChanged?.Invoke();
    }

    public void TakeDamage (float damage)
    {
        if (isAlive)
        {
            CurrentHealth -= damage;

            if (CurrentHealth <= 0)
            {
                CurrentHealth = 0;
                OnPersonDead?.Invoke();
            }
            OnHealthChanged?.Invoke();
        }
    }
}
