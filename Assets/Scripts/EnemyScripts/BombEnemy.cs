﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombEnemy : StandartEnemy
{
    protected override void Start()
    {
        base.Start();
        MoveToTarget();
    }
    
    void Update()
    {
        MoveToTarget(); 
    }

    private void MoveToTarget()
    {
        if (Vector3.Distance(transform.position, _localMain._mainHouse.transform.position) > _enemyConfig.enemyCharacteristics._attackZone)
        {
            _agent.SetDestination(_localMain._mainHouse.transform.position);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Attack(other.gameObject);
    }

    private void Attack(GameObject _target)
    {
        if (_target.tag == "MainHouse")
        {
            _target.GetComponent<Health>().TakeDamage(_enemyConfig.enemyCharacteristics._damage);
            Death();
        }
    }

}
