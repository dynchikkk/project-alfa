﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResourseBar : MonoBehaviour
{
    [SerializeField] private TMP_Text _resourseText;

    [SerializeField] private MineResources _mineResources;

    private void OnEnable()
    {
        _mineResources.OnMineResoursesChanged += OnMineResoursesChanged;
    }

    private void OnDisable()
    {
        _mineResources.OnMineResoursesChanged -= OnMineResoursesChanged;
    }

    private void OnMineResoursesChanged()
    {
        _resourseText.text = _mineResources._currentResourecesInStack.ToString();
    }
}
