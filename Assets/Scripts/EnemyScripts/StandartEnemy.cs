﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StandartEnemy : MonoBehaviour
{
    protected Game _localMain;
    public EnemyConfig _enemyConfig;

    //[SerializeField] public float _currentHp;
    [HideInInspector]public NavMeshAgent _agent;
    [HideInInspector] public float _currentDamageCd;

    private Health _health;

    private void Awake()
    {
        _health = GetComponent<Health>();
    }

    protected virtual void Start()
    {
        SetStartConstants();
    }

    protected virtual void OnEnable()
    {
        _health.OnPersonDead += Death;
    }

    protected virtual void OnDisable()
    {
        _health.OnPersonDead -= Death;
    }

    public void SetStartConstants()
    {
        _localMain = Game._main;
        _agent = GetComponent<NavMeshAgent>();
        _currentDamageCd = 0;
        _agent.speed = _enemyConfig.enemyCharacteristics._speed;
        _health.MaxHealth = _health.CurrentHealth = _enemyConfig.enemyCharacteristics._hp;
    }

    public void Death()
    {
        Destroy(gameObject);
    }
}
