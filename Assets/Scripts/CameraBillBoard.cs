﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Скрипт для поворота объекта в сторону камеры. Вешается в основном на hp бары

public class CameraBillBoard : MonoBehaviour
{
    private Camera _camera;

    private void Start()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        if (_camera != null)
        {
            Quaternion camQuat = _camera.transform.rotation;
            transform.rotation = camQuat;
        }
    }
}
