﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script for buuton that spawn towers
public class TowerPlaceButton : MonoBehaviour
{
    [HideInInspector] public int _towerNum;

    public void ChooseTower()
    {
        Game._main._lastTowerSpawner.GetComponent<TowerPlace>().InstantiateTower(_towerNum);
    }
}
