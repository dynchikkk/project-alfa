﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image _healthBarImage;
    [SerializeField] private Health _health;

    private Canvas _barCanvas;

    public bool IsBillBoard;
    public bool ShowOnAwake;

    private void Awake()
    {
        _barCanvas = GetComponent<Canvas>();
    }

    private void Start()
    {
        if (IsBillBoard)
            gameObject.AddComponent<CameraBillBoard>();

        if (ShowOnAwake == false)
            _barCanvas.enabled = false;
    }

    private void OnEnable()
    {
        _health.OnHealthChanged += UpdateHealthBar;
    }

    private void OnDisable()
    {
        _health.OnHealthChanged -= UpdateHealthBar;
    }

    private void UpdateHealthBar()
    {
        if (_barCanvas.enabled==false)
            _barCanvas.enabled = true;

        float healthAmount = _health.CurrentHealth / _health.MaxHealth;
        _healthBarImage.fillAmount = healthAmount;
    }
}
