﻿using System.Collections.Generic;
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "DefenceTowerConfig", menuName = "Configs/DefenceTowerConfig")]
public class DefenceTowerConfig : ScriptableObject
{
    public DefenceTowerCharacteristics defenceTowerCharacteristics;
}

[Serializable]
public class DefenceTowerCharacteristics
{
    [Header("Standart")]
    public string _name;
    public float _damage;
    public float _attackZone;
    public int _spawner;
    public int _resourcesForBuild;

    [Header("Attack")]
    // bullet speed
    public float _firePower;
    public float _attackCd;
    public float _rotatoinSpeed;
    public int _resourcesForShot;
}

