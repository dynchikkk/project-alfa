﻿using System.Collections.Generic;
using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "EnemyConfig", menuName = "Configs/EnemyConfig")]
public class EnemyConfig : ScriptableObject
{
    public EnemyCharacteristics enemyCharacteristics;
}

[Serializable]
public class EnemyCharacteristics
{
    [Header("Enemy constants")]
    public string name;
    //Max enemy hp
    public float _hp = 100;
    public float _attackZone = 10;
    public float _speed = 10;
    public float _damage = 10;
    //Max enemy attack cooldown
    public float _damageCd = 2;
}

