﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainHouse : MonoBehaviour
{
    private Game _localMain;

    private Health _health;

    private void Awake()
    {
        _health = GetComponent<Health>();
    }

    private void Start()
    {
        SetStartCharacteristic();
    }

    private void OnEnable()
    {
        _health.OnPersonDead += Death;
    }

    private void OnDisable()
    {
        _health.OnPersonDead -= Death;
    }

    private void SetStartCharacteristic()
    {
        _localMain = Game._main;
    }

    public void Death()
    {
        Destroy(gameObject, 1);
    }

    private void OnDestroy()
    {
        _localMain.ChangeScene("GameScene");
    }
}
